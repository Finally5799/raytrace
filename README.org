#+title: Readme

* README

Inspired by [[https://www.youtube.com/watch?v=nCIB8df7q2g][DConf'22: Ray Tracing in (Less Than) One Weekend with DLang -- Mike Shah]], following the [[https://raytracing.github.io/][Ray Tracing In One Weekend]] book

* Implementations
** C++ (Original based on book)
